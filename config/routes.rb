Rails.application.routes.draw do

  devise_for :users, controllers: {
                       sessions: 'auth/users/sessions' ,
                       registrations: 'auth/users/registrations',
                       passwords: 'auth/users/passwords'
                     }
  devise_scope :user do
    authenticated :user do
      root to: 'client/pages#dashboard', as: 'authenticated_user_root'
    end

    unauthenticated :user do
      root to: 'auth/users/sessions#new', as: 'unauthenticated_user_root'
    end
  end

  scope module: :client do
    resource  :account
    resources :items
    resources :link_specs,    only: [:show, :update]
    resources :offers,        only: [:index, :edit, :update]
    resources :messages,      only: [:new, :create]
    resources :conversations, only: [:index, :show, :destroy] do
      member do
        post :reply
        post :restore
        post :mark_as_read
      end
      collection do
        delete :empty_trash
      end
    end

    get 'dashboard', to: 'client/pages#dashboard'
  end

  namespace :storefront, path: 'spaces/:account_id' do
    resources :items,  only: [:show, :index]
    resources :offers, only: [:create]
  end

  # root to: 'client/pages#dashboard', as:
end

require 'uri'

class LinkSpec < ActiveRecord::Base
  belongs_to :item


  def generate_link(platform=nil)
    client = Bitly.client
    item_link = "http://offerspace.herokuapp.com/spaces/"\
                "#{self.item.account.id}/items/#{self.item.id}"
    url, field =
      case platform
      when 'facebook'
        ["#{item_link}?src=fb", 'fb_link']
      when 'twitter'
        ["#{item_link}?src=tw", 'tw_link']
      when 'instagram'
        ["#{item_link}?src=ig", 'ig_link']
      when 'google+'
        ["#{item_link}?src=gp", 'gp_link']
      else
        [item_link, 'd_link']
      end

    # check if link already exists
    if self.send(field).nil?
      short_url = client.shorten(url).short_url
      self.update_attribute(field, short_url)
    end
  end
end

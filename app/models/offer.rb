class Offer < ActiveRecord::Base
  STATUSES = %w(pending accepted rejected considering)

  belongs_to :item
  has_many :contact_info, dependent: :destroy

  monetize :amount_cents

  validates :status, inclusion: { in: STATUSES }

  scope :accepted, -> { where(status: 'accepted') }

  def source_humanized
    self.source
    case self.source
    when 'fb' then 'Facebook'
    when 'tw' then 'Twitter'
    when 'gp' then 'Google+'
    when 'ig' then 'Instagram'
    else
      self.source.presence || 'Not specified'
    end
  end
end

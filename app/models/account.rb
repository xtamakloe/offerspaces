class Account < ActiveRecord::Base
  has_many :users, dependent: :destroy
  has_many :items, dependent: :destroy
  has_many :offers, through: :items # account.offers.where('items.xx < ?', x)
  has_one  :social, dependent: :destroy
  accepts_nested_attributes_for :social

  has_attached_file :logo,
                    styles: { medium: '256x256>',
                              thumb: '60x60>',
                              large: '420x420>'},
                    default_url: ':style/placeholder_logo.png'

  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/
  # validates :name, presence: true


  after_create :create_social

  delegate :website, :facebook, :twitter, :instagram,
           to: :social, allow_nil: true


  private

  def create_social
    self.social = Social.create
  end
end

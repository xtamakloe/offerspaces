class ContactInfo < ActiveRecord::Base
  CHANNELS = %w(facebook twitter instagram email phone whatsapp)

  belongs_to :offer

  validates :channel, inclusion: { in: CHANNELS }


  def channel_humanized
    self.channel.titleize
  end

end

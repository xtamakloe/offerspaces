class User < ActiveRecord::Base
  acts_as_messageable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :account
  has_attached_file :avatar,
                    styles: { medium: '256x256>',
                              thumb: '60x60>'},
                    default_url: ':style/placeholder_user.gif'


  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/


  def full_name
    [ first_name, last_name ].join(' ')
  end


  def name
    first_name
  end


  def mailboxer_email(object)
    email
  end

end

class Item < ActiveRecord::Base
  belongs_to :account
  has_one :link_spec, dependent: :destroy

  monetize :unit_price_cents

  has_many :offers, dependent: :destroy
  has_attached_file :image,
                    styles: { medium: '256x256>',
                              thumb: '60x60>',
                              large: '420x420>'},
                    default_url: ':style/placeholder_item.gif'

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  validates :name, presence: true

  after_create :create_link_spec

  delegate :d_link,
           :fb_link,
           :tw_link,
           :gp_link,
           :ig_link, to: :link_spec, allow_nil: true


  def highest_offer
    self.offers.order(amount_cents: :desc).first
  end


  def accepted_offers
    self.offers.accepted
  end


  private

  def create_link_spec
    self.link_spec = LinkSpec.create
  end
end

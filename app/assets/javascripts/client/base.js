//= require jQuery/jQuery-2.1.3.min 
//= require jquery_ujs
//= require jQueryUI/jquery-ui-1.11.4.min
//= require bootstrap/js/bootstrap.min.js
//= require dist/js/app.min
// //= require plugins/bootstrap-timepicker.min
// //= require plugins/bootstrap-datepicker
// //= require plugins/jquery.jgrowl.min
//= require modal
//= require_self

// Resolve conflict in jQuery UI tooltip with Bootstrap tooltip 
$.widget.bridge('uibutton', $.ui.button);

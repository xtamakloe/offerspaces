module Client::ConversationsHelper
  def mailbox_section(title, current_box, opts = {})
    icon = opts.fetch(:icon, '')
    opts[:class] = opts.fetch(:class, '')
    opts[:class] += ' active' if title.downcase == current_box
    content_tag :li,
                link_to("<i class='fa fa-#{icon}' style='margin-right:10px'></i>#{title.capitalize}".html_safe,
                             conversations_path(box: title.downcase)), opts
  end
end

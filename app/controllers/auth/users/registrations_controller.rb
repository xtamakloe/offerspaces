class Auth::Users::RegistrationsController < Devise::RegistrationsController
  respond_to :html, :json

  layout 'sessions', only: [:new]

  def create
    super
    # Create account when user is created
    if resource.save
      account = Account.create #(name: resource.first_name)
      resource.update(account: account)
    # else
    #   flash[:notice] = flash[:notice].to_a.concat resource.errors.full_messages
    end
  end


  def edit
    respond_modal_with resource
  end


  # PUT /resource
  # We need to use a copy of the resource because we don't want to change
  # the current user in place.
  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, bypass: true
      # respond_with resource, location: after_update_path_for(resource)
      respond_modal_with resource, location: account_path
    else
      clean_up_passwords resource
      # respond_with resource
      respond_modal_with resource, location: account_path
    end
  end
end

class Auth::Users::SessionsController < Devise::SessionsController
  # layout false
  layout 'sessions', only: [:new]

  def after_sign_in_path_for(resource)
    authenticated_user_root_path
  end

  def after_sign_out_path_for(resource)
    new_user_session_path
  end
end

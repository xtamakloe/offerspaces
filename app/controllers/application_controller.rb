class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?


  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end


  protected

  def custom_devise_fields
    [:first_name, :last_name, :avatar]
  end


  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << custom_devise_fields
    devise_parameter_sanitizer.for(:account_update) << custom_devise_fields
  end
end

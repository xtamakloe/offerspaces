class Storefront::OffersController < ApplicationController
  def create
    item = Item.find(params[:offer][:item_id])
    contact_info = params[:contact_info]['0']
    # contact_present = contact_info.select{|d|d["address"].present?}.present?
    contact_present = contact_info['address'].present?

    account_name = item.account.name
    err_intro = "Sorry, your offer could not be sent to #{account_name} "

    if item
      if contact_present
        offer = item.offers.new(offer_params)
        if offer.save &&
          # contact_info.each do |ci|
          #   if ci['address'].present?
          #     offer.contact_info << ContactInfo.create(
          #       address: ci['address'],
          #       channel: ci['channel']
          #     )
          #   end
          # end
          offer.contact_info << ContactInfo.create(
                                  channel: contact_info['channel'],
                                  address: contact_info['address']
                                )
          flash[:success] = "Congratulations! <br/>Your offer has"\
                            " been sent to #{account_name}."\
                            #"While you wait for their reply, check out other "
        else
          flash[:error] = "#{err_intro} at this time. Please try again shortly."
        end
      else
        flash[:error] = "#{err_intro} because you didn't provide "\
                        "any contact details."
      end
    else
      flash[:error] = "Item not found."
    end
    redirect_to storefront_item_url(item, account_id: item.account.id)
  end


  private

  def offer_params
    params.require(:offer).permit(:amount,
                                  :quantity,
                                  :item_id,
                                  :source,
                                  :notes,
                                  :customer_name)
  end
end

class Storefront::ItemsController < ApplicationController
  before_filter :load_account

  layout 'storefront'

  def show
    @item     = @account.items.find(params[:id])
    @offer    = @item.offers.build
    @source   = params[:src]
  end


  def index
  end


  private

  def load_account
    @account ||= Account.find(params[:account_id])
  end
end

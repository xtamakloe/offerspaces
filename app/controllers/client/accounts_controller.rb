class Client::AccountsController < Client::BaseController
  respond_to :html, :json


  def edit
    respond_modal_with(current_account, location: authenticated_user_root_path)
  end


  def update
    if current_account.update_attributes(account_params)
      flash[:notice] = 'Account updated successfully.'
    else
      flash[:error] = current_account.errors.full_messages
    end
    redirect_to account_path
  end

  private

  def account_params
    # params.require(:account).permit!
    params.require(:account)
          .permit(:name,
                  :logo,
                  social_attributes:
                  [
                    :website,
                    :facebook,
                    :twitter,
                    :instagram
                  ])
  end
end

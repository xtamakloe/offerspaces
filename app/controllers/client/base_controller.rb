class Client::BaseController < ApplicationController
  before_filter :authenticate_user!

  helper_method :current_account

  layout 'client'

  def current_account
    @account ||= current_user.account
  end
end
class Client::ItemsController < Client::BaseController
  respond_to :html, :json


  def index
    @items = current_account.items
                            .paginate(page: params[:page])
                            .per_page(20)
                            .order(created_at: :desc)
  end


  def show
    @item = current_account.items.find(params[:id])
    @offers = @item.offers.order(created_at: :desc)
    @accepted_offers = @item.accepted_offers
  end


  def new
    @item = current_account.items.new
    respond_modal_with @item
  end


  def create
    @item = current_account.items.new(item_params)
    if @item.save
      flash[:success] = 'Item added successfully.'
      redirect_to @item
    else
      flash[:error] = @item.errors.full_messages
      redirect_to items_url
    end
  end


  def edit
    @item = current_account.items.find(params[:id])
    respond_modal_with @item
  end


  def update
    @item = current_account.items.find(params[:id])
    if @item.update_attributes(item_params)
      flash[:success] = 'Item updated successfully.'
    else
      flash[:error] = @item.errors.full_messages
    end
    redirect_to @item
  end


  def destroy
    item = current_account.items.find(params[:id])
    item.destroy
    flash[:success] = 'Item has been removed'
    redirect_to items_url
  end


  private

  def item_params
    params.require(:item).permit(:uid,
                                 :name,
                                 :unit_price,
                                 :active,
                                 :listing_url,
                                 :image)
  end
end

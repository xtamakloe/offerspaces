class Client::OffersController < Client::BaseController
  respond_to :html, :json

  def index
    @offers = current_account.offers
                             .paginate(page: params[:page])
                             .per_page(25)
                             .order(created_at: :desc)
  end


  def edit
    @offer = current_account.offers.find(params[:id])
    respond_modal_with @offer, location: authenticated_user_root_path
  end


  def update
    @offer = current_account.offers.find(params[:id])
    if @offer.update_attributes(offer_params)
      flash[:success] = 'Offer updated'
    else
      flash[:error] = @offer.errors.full_messages
    end
    redirect_to @offer.item
  end


  private

  def offer_params
    params.require(:offer).permit(:status)
  end
end

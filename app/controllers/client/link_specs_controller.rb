class Client::LinkSpecsController < Client::BaseController
  respond_to :html, :json


  def show
    @link_spec = LinkSpec.find(params[:id])
    @link = @link_spec.send(params[:link])

    respond_modal_with @link
  end


  def update
    @platform  = params[:platform]

    @link_spec = LinkSpec.find(params[:id])
    @link_spec.generate_link @platform

    respond_to do |format|
      format.js
    end
  end
end

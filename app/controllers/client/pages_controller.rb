class Client::PagesController < Client::BaseController
  def dashboard
    @latest_items  = current_account.items.order(created_at: :desc).limit(5)
    @latest_offers = current_account.offers.order(created_at: :desc).limit(5)
  end
end

class RenameLinkToDLinkForLinkSpecs < ActiveRecord::Migration
  def change
    rename_column :link_specs, :link, :d_link
  end
end

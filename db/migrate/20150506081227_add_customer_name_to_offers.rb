class AddCustomerNameToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :customer_name, :string
  end
end

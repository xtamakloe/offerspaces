class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.references :item, index: true
      t.monetize :amount

      t.timestamps null: false
    end
    add_foreign_key :offers, :items
  end
end

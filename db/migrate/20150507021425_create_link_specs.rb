class CreateLinkSpecs < ActiveRecord::Migration
  def change
    create_table :link_specs do |t|
      t.references :item, index: true
      t.string :link
      t.string :fb_link
      t.string :tw_link
      t.string :ig_link

      t.timestamps null: false
    end
    add_foreign_key :link_specs, :items
  end
end

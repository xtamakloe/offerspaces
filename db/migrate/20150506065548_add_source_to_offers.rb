class AddSourceToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :source, :string
  end
end

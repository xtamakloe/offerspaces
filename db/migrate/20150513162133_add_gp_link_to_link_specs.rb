class AddGpLinkToLinkSpecs < ActiveRecord::Migration
  def change
    add_column :link_specs, :gp_link, :string
  end
end

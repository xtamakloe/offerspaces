class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references :account, index: true
      t.string :name
      t.monetize :unit_price

      t.timestamps null: false
    end
    add_foreign_key :items, :accounts
  end
end

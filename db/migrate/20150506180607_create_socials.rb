class CreateSocials < ActiveRecord::Migration
  def change
    create_table :socials do |t|
      t.references :account, index: true
      t.string :facebook
      t.string :twitter
      t.string :instagram
      t.string :pinterest

      t.timestamps null: false
    end
    add_foreign_key :socials, :accounts
  end
end

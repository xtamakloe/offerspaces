class AddQuantityToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :quantity, :integer, default: 0
  end
end

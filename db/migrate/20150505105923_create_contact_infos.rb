class CreateContactInfos < ActiveRecord::Migration
  def change
    create_table :contact_infos do |t|
      t.references :offer, index: true
      t.string :channel
      t.string :address

      t.timestamps null: false
    end
    add_foreign_key :contact_infos, :offers
  end
end

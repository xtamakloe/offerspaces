class AddListingUrlToItems < ActiveRecord::Migration
  def change
    add_column :items, :listing_url, :string
  end
end

class AddWebsiteToSocials < ActiveRecord::Migration
  def change
    add_column :socials, :website, :string
  end
end

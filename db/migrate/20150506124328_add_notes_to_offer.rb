class AddNotesToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :notes, :string
  end
end
